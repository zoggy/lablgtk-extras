         Lablgtk-extras

*** What is Lablgtk-extras ***

This is a set of libraries useful when developing
ocaml/lablgtk3 applications.

*** Installation

Install "make install" or using opam with "opam install lablgtk3-extras".

*** Authors ***
Maxence Guesdon <maxence.guesdon@inria.fr>
