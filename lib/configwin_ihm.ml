(*********************************************************************************)
(*                Lablgtk-extras                                                 *)
(*                                                                               *)
(*    Copyright (C) 2011-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 3 of the         *)
(*    License.                                                                   *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** This module contains the gui functions of Configwin.*)

open Configwin_types

let file_html_config =
  let home =
    try Sys.getenv "HOME"
    with Not_found -> ""
  in
  Filename.concat home ".configwin_html"

let debug = true
let dbg = if debug then prerr_endline else (fun _ -> ())

(** Return the config group for the html config file,
   and the option for bindings. *)
let html_config_file_and_option () =
  let default =
    [ { html_key = Configwin_types.string_to_key "A-b" ;
        html_begin = "<b>";
        html_end = "</b>" ;
      } ;
      { html_key = Configwin_types.string_to_key "A-i" ;
        html_begin = "<i>";
        html_end = "</i>" ;
      }
    ]
  in
  let bindings = Ocf.list Configwin_types.htmlbinding_wrapper default in
  let ini = Ocf.add Ocf.group ["bindings"] bindings in
  Ocf.from_file ini file_html_config ;
  (ini, bindings)

(** This variable contains the last directory where the user selected a file.*)
let last_dir = ref "";;

(** This function allows the user to select a file and returns the
   selected file name. An optional function allows to change the
   behaviour of the ok button.
   A VOIR : mutli-selection ? *)
let select_files ?parent ?dir
    ?(fok : (string -> unit) option)
    the_title =
  let files = ref ([] : string list) in
  let dialog = GWindow.file_chooser_dialog ?parent
    ~action:`OPEN ~modal:true ~title: the_title ()
  in
  dialog#add_button_stock `CANCEL `CANCEL ;
  dialog#add_select_button_stock `OPEN `OPEN ;
  (* we set the previous directory, if no directory is given *)
  (
   match dir with
     None when !last_dir <> "" ->
         ignore(dialog#set_filename !last_dir)
   | Some dir -> ignore(dialog#set_filename !last_dir)
   | None -> ()
  );
  (
  match dialog#run () with
  | `DELETE_EVENT | `CANCEL -> ()
  | `OPEN ->
       match dialog#filename with
       | None -> files := []
       | Some fn ->
           match fok with
           | None -> files := [fn]
           | Some f -> f fn
  );
  dialog#destroy ();
  match !files with
  | [] | [""] -> []
  | l -> (* we keep the directory in last_dir *)
      last_dir := Filename.dirname (List.hd l);
      l
;;

(** Make the user select a date. *)
let select_date ?parent title (day,mon,year) =
  let dialog = GWindow.dialog ?parent ~modal:true ~title () in
  let cal = GMisc.calendar ~packing: (dialog#vbox#pack ~expand: true) () in
  cal#select_month ~month: mon ~year: year ;
  cal#select_day day;
  dialog#add_button_stock `OK `OK ;
  dialog#add_button_stock `CANCEL `CANCEL ;
  ignore(cal#connect#day_selected_double_click
   (fun () -> dialog#response `OK));
  let result =
    match dialog#run () with
    | `DELETE_EVENT | `CANCEL -> None
    | `OK -> Some cal#date
  in
  dialog#destroy ();
  result

(** This class builds a frame with a list and three buttons :
   one to add items, one to move up selected items,
   and one to remove the selected items.
   The class takes in parameter a function used to add items and
   a list ref which is used to store the content of the list.
   At last, a title for the frame  is also in parameter, so that
   each instance of the class creates a frame. *)
class ['a] list_selection_box
    (listref : 'a list ref)
    columns
    help_opt
    f_edit_opt
    f_color
    (eq : 'a -> 'a -> bool)
    add_function title editable
    =
  let _ = dbg "list_selection_box" in
  let wev = GBin.event_box () in
  let wf = GBin.frame ~label: title ~packing: wev#add () in
  let hbox = GPack.hbox ~packing: wf#add () in
  (* the scroll window and the clist *)
  let wscroll = GBin.scrolled_window
      ~vpolicy: `AUTOMATIC
      ~hpolicy: `AUTOMATIC
      ~packing: (hbox#pack ~expand: true) ()
  in
  let column_list = new GTree.column_list in
  let open Gobject.Data in
  let () = dbg "fg_column" in
  let fg_column = column_list#add (unsafe_boxed (Gobject.Type.from_name "GdkColor")) in
  let () = dbg "data_column" in
  let data_column = column_list#add Gobject.Data.caml in
  let _ = dbg "let model = ..." in

  let model_columns = List.map
    (fun (f_content, title) -> column_list#add Gobject.Data.string)
    columns
  in
  let model = GTree.list_store column_list in
  let _ = dbg "let view = ..." in
  let view = GTree.view ~model ~headers_visible:false
      ~packing:wscroll#add ()
  in
  let () = view#selection#set_mode `MULTIPLE in
  let () = List.iter2 (fun (f_content, title) c ->
       let renderer = GTree.cell_renderer_text [ `XALIGN 0.] in
       let vc = GTree.view_column ?title ~renderer:(renderer,["text",c]) () in
       vc#set_sizing `AUTOSIZE ;
       vc#add_attribute renderer "foreground-gdk" fg_column ;
       renderer#set_properties [`FOREGROUND_SET true] ;
       ignore(view#append_column vc);
       (match title with
        | None -> ()
        | Some s -> view#set_headers_visible true);
    )
    columns model_columns
  in
  let _ = match help_opt with
      None -> ()
    | Some help -> wev#set_tooltip_text help
  in
  let vbox_buttons = GPack.vbox () in
  let _ =
    if editable then
      let _ = hbox#pack ~expand: false vbox_buttons#coerce in
      ()
    else
      ()
  in
  let _ = dbg "list_selection_box: wb_add" in
  let wb_add = GButton.button
      ~stock:`ADD
      ~packing: (vbox_buttons#pack ~expand:false ~padding:2)
      ()
  in
  let wb_edit = GButton.button ~stock: `EDIT () in
  let _ = match f_edit_opt with
    None -> ()
  | Some _ -> vbox_buttons#pack ~expand:false ~padding:2 wb_edit#coerce
  in
  let wb_up = GButton.button
      ~stock:`GO_UP
      ~packing: (vbox_buttons#pack ~expand:false ~padding:2)
      ()
  in
  let wb_remove = GButton.button
      ~stock:`REMOVE
      ~packing: (vbox_buttons#pack ~expand:false ~padding:2)
      ()
  in
  let _ = dbg "list_selection_box: object(self)" in

  object (self)
    (** This method returns the frame created. *)
    method box = wev

    method update_row row elt =
      List.iter2
        (fun (f_content,_) column ->
           model#set ~row ~column (f_content elt))
        columns model_columns;
      model#set ~row ~column:data_column elt ;
      (match f_color elt with
       | None -> ()
       | Some c -> model#set ~row ~column:fg_column (GDraw.color (`NAME c)))

    method update_listref =
      let l = ref [] in
      model#foreach
        (fun path iter ->
          let elt = model#get ~row:iter ~column:data_column in
          l := elt :: !l ;
          false;
        );
      listref := List.rev !l

    method update l =
      model#clear ();
      List.iter
        (fun elt ->
           let row = model#append () in
           self#update_row row elt)
        l;
      self#update_listref

    method get_list_as_rr =
      let l = ref [] in
      model#foreach
        (fun path iter -> l := (model#get_row_reference path) :: !l; false);
      List.rev !l

    (** Move up the selected rows. *)
    method up_selected =
      let sel_paths = view#selection#get_selected_rows in
      let sel_rr = List.map (fun p -> model#get_row_reference p) sel_paths in
      let pred s rr2 = s = (GTree.Path.to_string rr2#path) in
      let rec iter = function
        []
      | [_] -> ()
      | rr :: rr2 :: q ->
          if List.exists (pred (GTree.Path.to_string rr2#path)) sel_rr then
            (
             ignore (model#move_before ~iter:rr2#iter ~pos:rr#iter);
             iter (rr :: q)
            )
          else
             iter (rr2 :: q)
      in
      iter self#get_list_as_rr;
      self#update_listref

    (** Make the user edit the first selected row. *)
    method edit_selected f_edit =
      match view#selection#get_selected_rows with
      | [] -> ()
      |	path :: _ ->
          try
            let rr = model#get_row_reference path in
            let row = model#get_iter path in
            let elt = model#get ~row ~column:data_column in
            let elt2 = f_edit elt in
            self#update_row row elt2;
            self#update_listref;
            view#selection#unselect_all ();
            view#selection#select_path rr#path
          with
            Not_found ->
              ()

    initializer
      (** create the functions called when the buttons are clicked *)
      let f_add () =
        (* get the files to add with the function provided *)
        let l = add_function () in
        (* remove from the list the ones which are already in
           the listref, using the eq predicate *)
        let l2 = List.fold_left
            (fun acc -> fun ele ->
              if List.exists (eq ele) acc then
                acc
              else
                acc @ [ele])
            !listref
            l
        in
        self#update l2
      in
      let f_remove () =
        (* remove the selected items from the listref and the list view *)
        let sel_paths = view#selection#get_selected_rows in
        let sel_rr = List.map (fun p -> model#get_row_reference p) sel_paths in
        List.iter (fun rr -> ignore(model#remove rr#iter)) sel_rr ;
        self#update_listref
      in
      let _ = dbg "list_selection_box: connecting wb_add" in
      (* connect the functions to the buttons *)
      ignore (wb_add#connect#clicked f_add);
      let _ = dbg "list_selection_box: connecting wb_remove" in
      ignore (wb_remove#connect#clicked f_remove);
      let _ = dbg "list_selection_box: connecting wb_up" in
      ignore (wb_up#connect#clicked (fun () -> self#up_selected));
      (
       match f_edit_opt with
       | None -> ()
       | Some f ->
           let _ = dbg "list_selection_box: connecting wb_edit" in
           ignore (wb_edit#connect#clicked (fun () -> self#edit_selected f))
      );
      (* initialize the list with the listref *)
      self#update !listref
  end;;


(** This class is used to build a box for a string parameter.*)
class string_param_box param =
  let _ = dbg "string_param_box" in
  let hbox = GPack.hbox () in
  let wev = GBin.event_box ~packing: (hbox#pack ~expand: false ~padding: 2) () in
  let _wl = GMisc.label ~text: param.string_label ~packing: wev#add () in
  let we = GEdit.entry
      ~editable: param.string_editable
      ~packing: (hbox#pack ~expand: param.string_expand ~padding: 2)
      ()
  in
  let _ =
    match param.string_help with
      None -> ()
    | Some help ->
        wev#misc#set_tooltip_text help
  in
  let () = we#set_text (param.string_to_string param.string_value) in

  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = param.string_of_string we#text in
      if new_value <> param.string_value then
        let () = param.string_f_apply new_value in
        param.string_value <- new_value
  end ;;

(** This class is used to build a box for a combo parameter.*)
class combo_param_box param =
  let _ = dbg "combo_param_box" in
  let hbox = GPack.hbox () in
  let wev = GBin.event_box ~packing: (hbox#pack ~expand: false ~padding: 2) () in
  let _wl = GMisc.label ~text: param.combo_label ~packing: wev#add () in
  let (wc, (store, column)) =
    let strings = param.combo_choices in
    GEdit.combo_box_entry_text ~strings
      ~packing: (hbox#pack ~expand: param.combo_expand ~padding: 2)
      ()
  in
  let _ =
    match param.combo_help with
      None -> ()
    | Some help -> wev#misc#set_tooltip_text help
  in
  let wecompl = GEdit.entry_completion ~model:store () in
  let () = wecompl#set_text_column column in
  let () = wc#entry#set_completion wecompl in
  let () = wc#misc#set_sensitive param.combo_editable in
  let () = wc#entry#set_editable param.combo_new_allowed in
  let () = wc#entry#set_text param.combo_value in

  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let v = wc#entry#text in
      if v <> param.combo_value then
        let () = param.combo_f_apply v in
        param.combo_value <- v
  end ;;

(** Class used to pack a custom box. *)
class custom_param_box param =
  let _ = dbg "custom_param_box" in
  let top =
    match param.custom_framed with
      None -> param.custom_box#coerce
    | Some l ->
	let wf = GBin.frame ~label: l () in
	wf#add param.custom_box#coerce;
	wf#coerce
  in
  object (self)
    method box = top
    method apply = param.custom_f_apply ()
  end

(** This class is used to build a box for a color parameter.*)
class color_param_box param =
  let _ = dbg "color_param_box" in
  let v = ref param.color_value in
  let hbox = GPack.hbox () in
  let _ = dbg (Printf.sprintf "color label = %S" param.color_label) in
  let _wl = GMisc.label ~text:param.color_label
    ~packing: (hbox#pack ~expand: false ~padding: 2) ()
  in
  let we = GEdit.entry
      ~editable: param.color_editable
      ~packing: (hbox#pack ~expand: param.color_expand ~padding: 2)
      ()
  in
  let wb =
    GButton.color_button ~title:param.color_label
    ~show: param.color_editable
    ~packing:(hbox#pack ~expand: false ~padding: 2)
    ()
  in
  let _ =
    match param.color_help with
      None -> ()
    | Some help -> wb#misc#set_tooltip_text help
  in
  let set_color s =
    try let c = Gdk.Color.color_parse s in wb#set_color c
    with _ -> ()
  in
  let () = set_color !v in
  let _ = we#set_text !v in
  let _ = wb#connect#color_set
    (fun () ->
       let s = Gdk.Color.color_to_string wb#color in
       v := s ;
       we#set_text s
    )
  in
  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = we#text in
      if new_value <> param.color_value then
        let () = param.color_f_apply new_value in
        param.color_value <- new_value
    initializer
      ignore (we#connect#changed (fun () -> set_color we#text))

  end ;;

(** This class is used to build a box for a font parameter.*)
class font_param_box param  =
  let _ = dbg "font_param_box" in
  let v = ref param.font_value in
  let hbox = GPack.hbox () in
  let _wl = GMisc.label ~text:param.font_label
    ~packing: (hbox#pack ~expand: false ~padding: 2) ()
  in
  let wb =
    GButton.font_button ~title:param.font_label
      ~font_name: !v
      ~packing:(hbox#pack ~expand: false ~padding: 2)
    ()
  in
  let () = wb#set_use_font true in
  let () = wb#misc#set_sensitive param.font_editable in
  let _ =
    match param.font_help with
      None -> ()
    | Some help -> wb#misc#set_tooltip_text help
  in
  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = wb#font_name in
      if new_value <> param.font_value then
        let () = param.font_f_apply new_value in
        param.font_value <- new_value
  end ;;

(** This class is used to build a box for a text parameter.*)
class text_param_box param =
  let _ = dbg "text_param_box" in
  let wf = GBin.frame ~label: param.string_label ~height: 100 () in
  let wev = GBin.event_box ~packing: wf#add () in
  let wscroll = GBin.scrolled_window
      ~vpolicy: `AUTOMATIC
      ~hpolicy: `AUTOMATIC
      ~packing: wev#add ()
  in
  let wview = GText.view
      ~editable: param.string_editable
      ~packing: wscroll#add
      ()
  in
  let _ =
    match param.string_help with
      None -> ()
    | Some help -> wev#misc#set_tooltip_text help
  in
  let _ = dbg "text_param_box: buffer creation" in
  let buffer = GText.buffer () in

  let _ = wview#set_buffer buffer in
  let _ = buffer#insert (param.string_to_string param.string_value) in
  let _ = dbg "text_param_box: object(self)" in
  object (self)
    val wview = wview
    (** This method returns the main box ready to be packed. *)
    method box = wf#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let v = param.string_of_string (buffer#get_text ()) in
      if v <> param.string_value then
        (
         dbg "apply new value !";
         let _ = param.string_f_apply v in
         param.string_value <- v
        )
  end ;;

(** This class is used to build a box a html parameter. *)
class html_param_box param =
  let _ = dbg "html_param_box" in
  object (self)
    inherit text_param_box param

    method private exec html_start html_end () =
      let (i1,i2) = wview#buffer#selection_bounds in
      let s = i1#get_text ~stop: i2 in
      match s with
      | "" -> wview#buffer#insert (html_start^html_end)
      |	_ ->
          ignore (wview#buffer#insert ~iter: i2 html_end);
          ignore (wview#buffer#insert ~iter: i1 html_start);
          wview#buffer#place_cursor ~where: i2

    initializer
      dbg "html_param_box:initializer";
      let (_,html_bindings) = html_config_file_and_option () in
      dbg "html_param_box:connecting key press events";
      let add_shortcut hb =
        let (mods, k) = hb.html_key in
        Okey.add wview ~mods k (self#exec hb.html_begin hb.html_end)
      in
      List.iter add_shortcut (Ocf.get html_bindings);
      dbg "html_param_box:end"
  end

(** This class is used to build a box for a boolean parameter.*)
class bool_param_box param =
  let _ = dbg "bool_param_box" in
  let wchk = GButton.check_button
      ~label: param.bool_label
      ()
  in
  let _ =
    match param.bool_help with
      None -> ()
    | Some help -> wchk#misc#set_tooltip_text help
  in
  let _ = wchk#set_active param.bool_value in
  let _ = wchk#misc#set_sensitive param.bool_editable in

  object (self)
    (** This method returns the check button ready to be packed. *)
    method box = wchk#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = wchk#active in
      if new_value <> param.bool_value then
        let () = param.bool_f_apply new_value in
        param.bool_value <- new_value
  end ;;

(** This class is used to build a box for a file name parameter.*)
class filename_param_box param =
  let _ = dbg "filename_param_box" in
  let hbox = GPack.hbox () in
  let wb = GButton.button ~label: param.string_label
      ~packing: (hbox#pack ~expand: false ~padding: 2) ()
  in
  let we = GEdit.entry
      ~editable: param.string_editable
      ~packing: (hbox#pack ~expand: param.string_expand ~padding: 2)
      ()
  in
  let _ =
    match param.string_help with
      None -> ()
    | Some help -> wb#misc#set_tooltip_text help
  in
  let _ = we#set_text (param.string_to_string param.string_value) in

  let f_click () =
    match select_files ?parent:(GWindow.toplevel hbox#coerce)
      param.string_label
    with
    | [] -> ()
    | f :: _ -> we#set_text f
  in
  let () = if param.string_editable then ignore(wb#connect#clicked f_click) in

  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = param.string_of_string we#text in
      if new_value <> param.string_value then
        let _ = param.string_f_apply new_value in
        param.string_value <- new_value
  end ;;

(** This class is used to build a box for a hot key parameter.*)
class hotkey_param_box param =
  let _ = dbg "hotkey_param_box" in
  let hbox = GPack.hbox () in
  let wev = GBin.event_box ~packing: (hbox#pack ~expand: false ~padding: 2) () in
  let _wl = GMisc.label ~text: param.hk_label
      ~packing: wev#add ()
  in
  let we = GEdit.entry
      ~editable: false
      ~packing: (hbox#pack ~expand: param.hk_expand ~padding: 2)
      ()
  in
  let value = ref param.hk_value in
  let _ =
    match param.hk_help with
      None -> ()
    | Some help -> wev#misc#set_tooltip_text help
  in
  let _ = we#set_text (Configwin_types.key_to_string param.hk_value) in
  let mods_we_dont_care = [`MOD2 ; `MOD3 ; `MOD4 ; `MOD5 ; `LOCK] in
  let capture ev =
    let key = GdkEvent.Key.keyval ev in
    let modifiers = GdkEvent.Key.state ev in
    let mods = List.filter
      (fun m -> not (List.mem m mods_we_dont_care))
        modifiers
    in
    value := (mods, key);
    we#set_text (Glib.Convert.locale_to_utf8 (Configwin_types.key_to_string !value));
    false
  in
  let () =
    if param.hk_editable then
      ignore (we#event#connect#key_press capture)
  in
  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let new_value = !value in
      if new_value <> param.hk_value then
        let () = param.hk_f_apply new_value in
        param.hk_value <- new_value
  end ;;

(** This class is used to build a box for a date parameter.*)
class date_param_box param =
  let _ = dbg "date_param_box" in
  let v = ref param.date_value in
  let hbox = GPack.hbox () in
  let wb = GButton.button ~label: param.date_label
      ~packing: (hbox#pack ~expand: false ~padding: 2) ()
  in
  let we = GEdit.entry
      ~editable: false
      ~packing: (hbox#pack ~expand: param.date_expand ~padding: 2)
      ()
  in
  let () =
    match param.date_help with
    | None -> ()
    | Some help -> wb#misc#set_tooltip_text help
  in

  let () = we#set_text (param.date_f_string param.date_value) in
  let f_click () =
    match select_date param.date_label !v with
    | None -> ()
    | Some (y,m,d) ->
        v := (d,m,y) ;
        we#set_text (param.date_f_string (d,m,y))
  in
  let () = if param.date_editable then ignore(wb#connect#clicked f_click) in
  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = hbox#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      if !v <> param.date_value then
        let () = param.date_f_apply !v in
        param.date_value <- !v
  end ;;

(** This class is used to build a box for a parameter whose values are a list.*)
class ['a] list_param_box (param : 'a list_param) =
  let _ = dbg "list_param_box" in
  let listref = ref param.list_value in
  let frame_selection = new list_selection_box
      listref
      param.list_columns
      param.list_help
      param.list_f_edit
      param.list_color
      param.list_eq
      param.list_f_add param.list_label param.list_editable
  in

  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = frame_selection#box#coerce
    (** This method applies the new value of the parameter. *)
    method apply =
      let c e1 e2 = if param.list_eq e1 e2 then 0 else 1 in
      if List.compare c param.list_value !listref <> 0 then
        let () = param.list_f_apply !listref in
        param.list_value <- !listref
  end ;;

(** This class is used to build a box from a configuration structure
   and adds the page to the given notebook. *)
class configuration_box conf_struct (notebook : GPack.notebook) =
  (* we build different widgets, according to the conf_struct parameter *)
  let main_box = GPack.vbox () in
  let (label, child_boxes) =
    match conf_struct with
      Section (label, param_list) ->
        let f parameter =
          match parameter with
          | String_param p ->
              let box = new string_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Combo_param p ->
              let box = new combo_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Text_param p ->
              let box = new text_param_box p in
              let _ = main_box#pack ~expand: p.string_expand ~padding: 2 box#box in
              box
          | Bool_param p ->
              let box = new bool_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Filename_param p ->
              let box = new filename_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | List_param f ->
              let box = f () in
              let _ = main_box#pack ~expand: true ~padding: 2 box#box in
              box
          | Custom_param p ->
              let box = new custom_param_box p in
              let _ = main_box#pack ~expand: p.custom_expand ~padding: 2 box#box in
              box
          | Color_param p ->
              let box = new color_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Font_param p ->
              let box = new font_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Date_param p ->
              let box = new date_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Hotkey_param p ->
              let box = new hotkey_param_box p in
              let _ = main_box#pack ~expand: false ~padding: 2 box#box in
              box
          | Html_param p ->
              let box = new html_param_box p in
              let _ = main_box#pack ~expand: p.string_expand ~padding: 2 box#box in
              box
        in
        let list_children_boxes = List.map f param_list in
        (label, list_children_boxes)

    | Section_list (label, struct_list) ->
        let wnote = GPack.notebook
          (*homogeneous_tabs: true*)
          ~scrollable: true
            ~show_tabs: true
            ~packing: (main_box#pack ~expand: true)
            ()
        in
        (* we create all the children boxes *)
        let f structure =
          let new_box = new configuration_box structure wnote in
          new_box
        in
        let list_child_boxes = List.map f struct_list in
        (label, list_child_boxes)
  in
  let page_label = GMisc.label ~text: label () in
  let _ = notebook#append_page
      ~tab_label: page_label#coerce
      main_box#coerce
  in
  object (self)
    (** This method returns the main box ready to be packed. *)
    method box = main_box#coerce
    (** This method make the new values of the paramters applied, recursively in
       all boxes.*)
    method apply =
      List.iter (fun box -> box#apply) child_boxes
  end
;;

(** Create a vbox with the list of given configuration structure list,
   and the given list of buttons (defined by their label and callback).
   Before calling the callback of a button, the [apply] function
   of each parameter is called.
*)
let tabbed_box conf_struct_list buttons =
  let vbox = GPack.vbox () in
  let wnote = GPack.notebook
    ~scrollable: true
      ~show_tabs: true
      ~packing: (vbox#pack ~expand: true)
      ()
  in
  let list_param_box =
    List.map
      (fun conf_struct -> new configuration_box conf_struct wnote)
      conf_struct_list
  in
  let f_apply () =
    List.iter (fun param_box -> param_box#apply) list_param_box  ;
  in
  let hbox_buttons = GPack.hbox ~packing: (vbox#pack ~expand: false ~padding: 4) () in
  let rec iter_buttons ?(grab=false) = function
  | [] -> ()
  | (label, callb) :: q ->
      let b = GButton.button ~label: label
        ~packing:(hbox_buttons#pack ~expand:true ~fill: true ~padding:4) ()
      in
      ignore (b#connect#clicked ~callback: (fun () -> f_apply (); callb ()));
      (* If it's the first button then give it the focus *)
      if grab then b#grab_default ();
      iter_buttons q
  in
  iter_buttons ~grab: true buttons;
  vbox

(** This function takes a configuration structure list and creates a window
   to configure the various parameters. *)
let edit ?parent ?(with_apply=true)
    ?(apply=(fun () -> ()))
    title ?(width=400) ?(height=400)
    conf_struct_list =
  let dialog = GWindow.dialog ?parent
      ~modal: true ~title: title
      ~height ~width ()
  in
  let wnote = GPack.notebook
    ~scrollable: true ~show_tabs: true
      ~packing: (dialog#vbox#pack ~expand: true) ()
  in
  let list_param_box =
    List.map
      (fun conf_struct -> new configuration_box conf_struct wnote)
      conf_struct_list
  in

  if with_apply then dialog#add_button_stock `APPLY `APPLY;
  dialog#add_button_stock `OK `OK;
  dialog#add_button_stock `CANCEL `CANCEL;

  let f_apply () =
    List.iter (fun param_box -> param_box#apply) list_param_box  ;
    apply ()
  in
  let f_ok () =
    List.iter (fun param_box -> param_box#apply) list_param_box  ;
    Return_ok
  in
  let rec iter rep =
    try
      match dialog#run () with
      | `APPLY  -> f_apply (); iter Return_apply
      | `OK -> f_ok ()
      | _ -> rep
    with
      Failure s -> GToolbox.message_box "Error" s; iter rep
    | e -> GToolbox.message_box "Error" (Printexc.to_string e); iter rep
  in
  let r = iter Return_cancel in
  dialog#destroy ();
  r

(** Create a vbox with the list of given parameters. *)
let box param_list =
  let main_box = GPack.vbox  () in
  let f parameter =
    match parameter with
    | String_param p ->
        let box = new string_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Combo_param p ->
        let box = new combo_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Text_param p ->
        let box = new text_param_box p in
        let _ = main_box#pack ~expand: p.string_expand ~padding: 2 box#box in
        box
    | Bool_param p ->
        let box = new bool_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Filename_param p ->
        let box = new filename_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | List_param f ->
        let box = f () in
        let _ = main_box#pack ~expand: true ~padding: 2 box#box in
        box
    | Custom_param p ->
        let box = new custom_param_box p in
        let _ = main_box#pack ~expand: p.custom_expand ~padding: 2 box#box in
        box
    | Color_param p ->
        let box = new color_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Font_param p ->
        let box = new font_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Date_param p ->
        let box = new date_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Hotkey_param p ->
        let box = new hotkey_param_box p in
        let _ = main_box#pack ~expand: false ~padding: 2 box#box in
        box
    | Html_param p ->
        let box = new html_param_box p in
        let _ = main_box#pack ~expand: p.string_expand ~padding: 2 box#box in
        box
  in
  let list_param_box = List.map f param_list in
  let f_apply () =
    List.iter (fun param_box -> param_box#apply) list_param_box
  in
  (main_box, f_apply)

(** This function takes a list of parameter specifications and
   creates a window to configure the various parameters.*)
let simple_edit ?parent ?(with_apply=true)
    ?(apply=(fun () -> ()))
    title ?width ?height
    param_list =
  let dialog = GWindow.dialog ?parent
      ~modal: true ~title: title
      ?height ?width ()
  in
  if with_apply then dialog#add_button_stock `APPLY `APPLY;
  dialog#add_button_stock `OK `OK;
  dialog#add_button_stock `CANCEL `CANCEL;

  let (box, f_apply) = box param_list in
  dialog#vbox#pack ~expand: true ~fill: true box#coerce;
  let rec iter rep =
    try
      match dialog#run () with
      | `APPLY  -> f_apply (); apply (); iter Return_apply
      | `OK -> f_apply () ; Return_ok
      | _ -> rep
    with
      Failure s ->
        GToolbox.message_box "Error" s; iter rep
    | e ->
        GToolbox.message_box "Error" (Printexc.to_string e); iter rep
  in
  let r = iter Return_cancel in
  dialog#destroy();
  r

let edit_string label s =
  match GToolbox.input_string ~title:label ~text: s label with
    None -> s
  | Some s2 -> s2

(** Create a string param. *)
let string ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  String_param
    {
      string_label = label ;
      string_help = help ;
      string_value = v ;
      string_editable = editable ;
      string_f_apply = f ;
      string_expand = expand ;
      string_to_string = (fun x -> x) ;
      string_of_string = (fun x -> x) ;
    }

(** Create a custom string param. *)
let custom_string ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) ~to_string ~of_string label v =
  String_param
    (Configwin_types.mk_custom_text_string_param
     {
       string_label = label ;
       string_help = help ;
       string_value = v ;
       string_editable = editable ;
       string_f_apply = f ;
       string_expand = expand ;
       string_to_string = to_string;
       string_of_string = of_string ;
     }
    )

(** Create a bool param. *)
let bool ?(editable=true) ?help ?(f=(fun _ -> ())) label v =
  Bool_param
    {
      bool_label = label ;
      bool_help = help ;
      bool_value = v ;
      bool_editable = editable ;
      bool_f_apply = f ;
    }

(** Create a list param. *)
let list ?(editable=true) ?help
    ?(f=(fun (_:'a list) -> ()))
    ?(eq=Stdlib.(=))
    ?(edit:('a -> 'a) option)
    ?(add=(fun () -> ([] : 'a list)))
    ?(color=(fun (_:'a) -> (None : string option)))
    label (columns : (('a -> string) * string option) list) v =
  List_param
    (fun () ->
       Obj.magic
         (new list_param_box
          {
            list_label = label ;
            list_help = help ;
            list_value = v ;
            list_editable = editable ;
            list_columns = columns ;
            list_eq = eq ;
            list_color = color ;
            list_f_edit = edit ;
            list_f_add = add ;
            list_f_apply = f ;
          }
         )
    )

(** Create a strings param. *)
let strings ?(editable=true) ?help
    ?(f=(fun _ -> ()))
    ?(eq=Stdlib.(=))
    ?(add=(fun () -> [])) label v =
  list ~editable ?help ~f ~eq ~edit: (edit_string label) ~add label
    [(fun s -> s), None] v

(** Create a color param. *)
let color ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  Color_param
    {
      color_label = label ;
      color_help = help ;
      color_value = v ;
      color_editable = editable ;
      color_f_apply = f ;
      color_expand = expand ;
    }

(** Create a font param. *)
let font ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  Font_param
    {
      font_label = label ;
      font_help = help ;
      font_value = v ;
      font_editable = editable ;
      font_f_apply = f ;
      font_expand = expand ;
    }

(** Create a combo param. *)
let combo ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ()))
    ?(new_allowed=false)
    label choices v =
  Combo_param
    {
      combo_label = label ;
      combo_help = help ;
      combo_value = v ;
      combo_editable = editable ;
      combo_choices = choices ;
      combo_new_allowed = new_allowed ;
      combo_f_apply = f ;
      combo_expand = expand ;
    }

(** Create a text param. *)
let text ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  Text_param
    {
      string_label = label ;
      string_help = help ;
      string_value = v ;
      string_editable = editable ;
      string_f_apply = f ;
      string_expand = expand ;
      string_to_string = (fun x -> x) ;
      string_of_string = (fun x -> x) ;
    }

(** Create a custom text param. *)
let custom_text ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) ~to_string ~of_string label v =
  Text_param
    (Configwin_types.mk_custom_text_string_param
     {
       string_label = label ;
       string_help = help ;
       string_value = v ;
       string_editable = editable ;
       string_f_apply = f ;
       string_expand = expand ;
       string_to_string = to_string;
       string_of_string = of_string ;
       }
    )

(** Create a html param. *)
let html ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  Html_param
    {
      string_label = label ;
      string_help = help ;
      string_value = v ;
      string_editable = editable ;
      string_f_apply = f ;
      string_expand = expand ;
      string_to_string = (fun x -> x) ;
      string_of_string = (fun x -> x) ;
    }

(** Create a filename param. *)
let filename ?(editable=true) ?(expand=true)?help ?(f=(fun _ -> ())) label v =
  Filename_param
    {
      string_label = label ;
      string_help = help ;
      string_value = v ;
      string_editable = editable ;
      string_f_apply = f ;
      string_expand = expand ;
      string_to_string = (fun x -> x) ;
      string_of_string = (fun x -> x) ;
    }

(** Create a filenames param.*)
let filenames ?(editable=true) ?help ?(f=(fun _ -> ()))
    ?(eq=Stdlib.(=))
    label v =
  let add () = select_files label in
  list ~editable ?help ~f ~eq ~add label
    [(fun s -> Glib.Convert.locale_to_utf8 s), None] v

(** Create a date param. *)
let date ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ()))
    ?(f_string=(fun(d,m,y)-> Printf.sprintf "%d/%d/%d" y (m+1) d))
    label v =
  Date_param
    {
      date_label = label ;
      date_help = help ;
      date_value = v ;
      date_editable = editable ;
      date_f_string = f_string ;
      date_f_apply = f ;
      date_expand = expand ;
    }

(** Create a hot key param. *)
let hotkey ?(editable=true) ?(expand=true) ?help ?(f=(fun _ -> ())) label v =
  Hotkey_param
    {
      hk_label = label ;
      hk_help = help ;
      hk_value = v ;
      hk_editable = editable ;
      hk_f_apply = f ;
      hk_expand = expand ;
    }

(** Create a custom param.*)
let custom ?label box f expand =
  Custom_param
    {
      custom_box = box ;
      custom_f_apply = f ;
      custom_expand = expand ;
      custom_framed = label ;
    }
