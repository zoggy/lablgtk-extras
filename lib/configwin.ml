(*********************************************************************************)
(*                Lablgtk-extras                                                 *)
(*                                                                               *)
(*    Copyright (C) 2011-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 3 of the         *)
(*    License.                                                                   *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

type parameter_kind = Configwin_types.parameter_kind

type configuration_structure =
    Configwin_types.configuration_structure =
    Section of string * parameter_kind list
  | Section_list of string * configuration_structure list

type return_button =
    Configwin_types.return_button =
    Return_apply
  | Return_ok
  | Return_cancel

let string_to_key = Configwin_types.string_to_key
let key_to_string = Configwin_types.key_to_string
let key_wrapper = Configwin_types.key_wrapper
let key_option = Configwin_types.key_option


let string = Configwin_ihm.string
let custom_string = Configwin_ihm.custom_string
let text = Configwin_ihm.text
let custom_text = Configwin_ihm.custom_text
let strings = Configwin_ihm.strings
let list = Configwin_ihm.list
let bool = Configwin_ihm.bool
let filename = Configwin_ihm.filename
let filenames = Configwin_ihm.filenames
let color = Configwin_ihm.color
let font = Configwin_ihm.font
let combo = Configwin_ihm.combo
let custom = Configwin_ihm.custom
let date = Configwin_ihm.date
let hotkey = Configwin_ihm.hotkey
let html = Configwin_ihm.html

let edit ?parent
    ?(apply=(fun () -> ()))
    title ?(width=400) ?(height=400)
    conf_struct_list =
  Configwin_ihm.edit ?parent ~with_apply: true ~apply title ~width ~height conf_struct_list

let get = Configwin_ihm.edit ~with_apply: false ~apply: (fun () -> ())

let simple_edit ?parent
    ?(apply=(fun () -> ()))
    title ?width ?height param_list =
    Configwin_ihm.simple_edit
    ?parent ~with_apply: true ~apply title ?width ?height param_list

let simple_get ?parent = Configwin_ihm.simple_edit ?parent
    ~with_apply: false ~apply: (fun () -> ())

let box = Configwin_ihm.box

let tabbed_box = Configwin_ihm.tabbed_box
