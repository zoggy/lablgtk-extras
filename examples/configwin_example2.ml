(*********************************************************************************)
(*                Lablgtk-extras                                                 *)
(*                                                                               *)
(*    Copyright (C) 2011-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 3 of the         *)
(*    License.                                                                   *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** *)

(* Compile with
  ocamlfind ocamlc -package lablgtk3-extras.configwin -linkpkg \
  -o configwin_example2.x <this_file>
*)

let _ = GMain.Main.init ()

open Configwin

let param1 = string ~help: "a string" "a string" "a value"
let param2 = bool ~help: "bool value" "a boolean" true
let param3 = filename ~help: "a file name" "a file name" "foo"
let param4 = strings
    ~help: "a list of strings"
    ~eq: (fun _ -> fun _ -> false)
    ~add: (fun () -> ["another string" ; "and another string"])
    "a string list"
    ["foo" ; "bar"]
let param5 = color
  ~f: (fun c -> prerr_endline (Printf.sprintf "Selected color: %S" c))
  ~help: "Pickup a color" "a color" "Red"
let param6 = font
  ~f: (fun font -> prerr_endline (Printf.sprintf "Selected font: %S" font))
  ~help: "Choose a font" "a font" "Monospace 12"
let param7 = date
  ~f:(fun (d,m,y) -> prerr_endline (Printf.sprintf "Selected date: %d/%d/%d" d m y))
  ~help: "Choose a date" "a date" (1, 0, 2021)
let n = ref 0
let param8 = list
    ~help: "a list of int"
    ~f:(fun l -> prerr_endline (String.concat ", " (List.map string_of_int l)))
    ~add: (fun () -> incr n; [!n])
    ~color: (fun n -> if n mod 2 = 0 then Some "red" else Some "green")
    "an int list"
    [ (fun n -> string_of_int n), Some "n" ;
      (fun n -> string_of_int (n*n)), Some "n*n" ;
    ]
    [1 ; 2 ; 3]
let param9 = filenames ~help: "a list of filenames" "filenames" []

let param10 = combo ~help:"Make a choice !"
  ~f:(fun s -> prerr_endline (Printf.sprintf "Selected pizza: %s" s))
    ~new_allowed:true
    "pizza"
    ["Margarita";"Regina";"Calzone"]
    "Calzone"

let structure = Section_list
  ("Section 1",
   [
    Section ("Section 1.1",
             [ param1 ; param2 ; param5 ; param6 ; param7 ; param9]);
    Section ("Section 1.2",
             [ param3 ; param4 ; param8 ; param10])
   ]
  )

let _ = Configwin.edit "Titre" [structure]
