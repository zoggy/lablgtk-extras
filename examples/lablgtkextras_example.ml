(*********************************************************************************)
(*                Lablgtk-extras                                                 *)
(*                                                                               *)
(*    Copyright (C) 2011-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Library General Public License as            *)
(*    published by the Free Software Foundation; either version 3 of the         *)
(*    License.                                                                   *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Library General Public          *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** *)

let _ = GMain.Main.init ()

type pizza = { name: string; price: float; vege: bool }

class pizza_box () =
  let columns =
    [ Some "Name", Gmylist.String (fun t -> t.name) ;
      Some "Vegetarian", Gmylist.Check ((fun p -> p.vege), (fun p _ -> p.vege)) ;
      Some "Price", Gmylist.String (fun t -> Printf.sprintf "%.1f€" t.price) ;
    ]
  in
  object(self)
    inherit [pizza] Gmylist.plist `SINGLE columns true as plist
    method menu = [`I ("Order", (fun () -> prerr_endline "Ok"))]
  end

type file = string (* absolute path *)

let dir_entries f =
  let open Unix in
  try match (stat f).st_kind with
  | S_DIR ->
        begin
          let h = opendir f in
          let rec iter acc =
            match readdir h with
            | exception End_of_file -> List.rev acc
            | s when s = Filename.current_dir_name || s = Filename.parent_dir_name -> iter acc
            | s -> iter ((Filename.concat f s)::acc)
          in
          let l = iter [] in
          closedir h;
          l
        end
  | _ -> []
  with Unix.Unix_error _ -> []

class filetree_box () =
  let root = "/tmp" in
  let f_contents f = [`String (Filename.basename f)] in
  object(tree)
    inherit [file] Gmytree.tree_edit
      ~f_roots:(fun() -> [root])
      ~f_children:dir_entries
      ~f_contents [`String ""]
  end

class dir_box () =
  object(self)
    inherit Gdir.gdir ()
    method roots = ["/tmp" ; "." ]
    method on_expand_dir d = prerr_endline (Printf.sprintf "%s expanded!" d)
    method on_collapse_dir d = prerr_endline (Printf.sprintf "%s collapsed!" d)
    method on_select_dir d = prerr_endline (Printf.sprintf "%s selected!" d)
    method on_unselect_dir d = prerr_endline (Printf.sprintf "%s unselected!" d)

  end

let w = GWindow.window
  ~title:"Lablgtk-extras example" ~width: 800 ~height: 600 ~show:true()
let () = ignore (w#connect#destroy GMain.Main.quit)
let vbox = GPack.vbox ~packing:w#add ()
let bindings_info = GMisc.label
  ~xalign: 0. ~xpad:5
  ~text: "Bindings:\nC-x C-q: quit\nC-x C-a: display message"
  ~packing:(vbox#pack ~expand:false) ()

let nb = GPack.notebook ~show_tabs:true ~packing:(vbox#pack ~expand:true) ()
let add_page text w =
  let label = GMisc.label ~text () in
  ignore(nb#append_page ~tab_label:label#coerce w#coerce)

let pizza_box = new pizza_box ()
let () =
  let p name price vege = { name ; price ; vege } in
  pizza_box#update_data
    [ p "Regina" 12. true ; p "Calzone" 14. false ; p "Margarita" 12. false ;
      p "Nordish" 15. false ; p "Burgundy" 14.5 false ;
    ]
let () = add_page "Gmylist" pizza_box#box

let filetree_box = new filetree_box ()
let () = add_page "Gmytree" filetree_box#box

let bindings =
  let ks = List.map Configwin.string_to_key in
  Okey.trees_of_list
    [ ks [ "C-x" ; "C-q" ], GMain.Main.quit ;
      ks [ "C-x" ; "C-a" ],
      (fun () -> GToolbox.message_box ~parent:w
         ~title:"Lablgtk-extras example" "Hello world !" ) ;
    ]
let () = Okey.set_handler_trees (fun() -> bindings) w

let dir_box = new dir_box ()
let () = add_page "Gdir" dir_box#box#coerce

let sv_pane = GPack.paned `VERTICAL ()
let sv_vbox = GPack.vbox ~packing:sv_pane#add1 ()
let sv_views = GPack.paned `VERTICAL ~packing:sv_pane#add2()

let language_manager = Gtksv_utils.source_language_manager
let () = Gtksv_utils.(set_source_style_scheme (read_style_scheme_selection ()))

let sv_props = Gtksv_utils.read_sourceview_props ()

let create_source_view ?editable ?packing () =
  let lang = language_manager#guess_language
    ~content_type:"text/x-ocaml" ()
  in
  let source_view =
    GSourceView3.source_view
      ?editable
      ~auto_indent:true
      ~insert_spaces_instead_of_tabs:true ~tab_width:2
      ~show_line_numbers:false
      ~smart_home_end: `ALWAYS
      ?packing
      ()
  in
  source_view#source_buffer#set_language lang;
  source_view#source_buffer#set_highlight_syntax true;
  (* set a style for bracket matching *)
  source_view#source_buffer#set_highlight_matching_brackets true;
  Gtksv_utils.register_source_buffer source_view#source_buffer ;
  Gtksv_utils.register_source_view source_view;
  Gtksv_utils.apply_sourceview_props source_view sv_props ;
  source_view

let sv_scheme_box = new Gtksv_utils.source_style_scheme_box ()
let () = sv_vbox#pack ~expand:false sv_scheme_box#box

let sv_wb_store_scheme = GButton.button ~label: "Store scheme choice"
  ~packing:(sv_vbox#pack ~expand:false) ()
let () = ignore(sv_wb_store_scheme#connect#clicked
  (fun () -> Gtksv_utils.store_style_scheme_selection sv_scheme_box#scheme))

let sv_props_box = new Gtksv_utils.sourceview_props_box
  Gtksv_utils.apply_sourceview_props_to_registered
let () = sv_props_box#set_props (Some sv_props)
let () = sv_vbox#pack ~expand:true sv_props_box#box

let sv_wb_store = GButton.button ~label: "Store preferences"
  ~packing:(sv_vbox#pack ~expand:false) ()
let () = ignore(sv_wb_store#connect#clicked
  (fun () ->
    match sv_props_box#props with
    | None -> ()
    | Some p -> Gtksv_utils.store_sourceview_props p))

let sv_scroll1 = GBin.scrolled_window ~packing:sv_views#add1 ()
let sv_scroll2 = GBin.scrolled_window ~packing:sv_views#add2 ()
let sv1 = create_source_view ~packing:sv_scroll1#add ()
let () = sv1#buffer#insert "let f x = x + 1"
let sv2 = create_source_view ~packing:sv_scroll2#add ()
let () = sv2#buffer#insert "type t =\n| Foo of int\n| Bar of string\n"
let () = Gtksv_utils.apply_sourceview_props_to_registered sv_props

let () = add_page "Gtksv_utils" sv_pane#coerce

let () = GMain.Main.main()

